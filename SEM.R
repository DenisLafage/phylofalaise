# Libraries


library(piecewiseSEM)
library(tidyverse)
library(nlme)
library(emmeans)
library(modelsummary)




##################################################"
# New models with results of Bayes including Ellenberg
####################################################"

# Spiders

## With response var from BAT

SEM_S0 <- psem(lme(mean_PD ~ bare_ground + HILL_F + HILL_S + mean_alpha, random = ~ 1|Site, data= div_spid3),
                   lme(mean_FD ~ mean_alpha + mean_PD, random = ~ 1|Site, data= div_spid3),
                   lme(mean_alpha ~ HILL_F + HILL_S , random = ~ 1|Site, data= div_spid3),
                   mean_PD %~~% mean_FD
)


summary(SEM_S0)


SEM_S1 <- psem(lme(mean_PD ~ mean_alpha, random = ~ 1|Site, data= div_spid3),
               lme(mean_FD ~ mean_PD, random = ~ 1|Site, data= div_spid3),
               lme(mean_alpha ~ HILL_S+ HILL_F, random = ~ 1|Site, data= div_spid3)
)


summary(SEM_S1)

## With MPD vars (picante)
SEM_S0_bis <- psem(lme(mpd.obs.z ~ H_lit  + mean_alpha, random = ~ 1|Site, data= div_spid2),
                   lme(mfd.obs ~ mean_alpha + mpd.obs.z+ H_lit, random = ~ 1|Site, data= div_spid2),
                   lme(mean_alpha ~ HILL_F + HILL_S , random = ~ 1|Site, data= div_spid2),
                   mpd.obs.z %~~% mfd.obs
)


summary(SEM_S0_bis)


## With NTI var (picante)
SEM_S0_nti <- psem(lme(mntd.obs.z ~ H_lit  + mean_alpha, random = ~ 1|Site, data= div_spid2),
                   lme(mntd.obs.F ~ mean_alpha + mntd.obs.z+ H_lit, random = ~ 1|Site, data= div_spid2),
                   lme(mean_alpha ~ HILL_F + HILL_S , random = ~ 1|Site, data= div_spid2),
                   mpd.obs.z %~~% mfd.obs
)


summary(SEM_S0_nti)



# Ants

SEM_A0 <- psem(lme(mean_PD ~ Soil_depth + mean_alpha, random = ~ 1|Site, data= div_ants2),
               lme(mean_FD ~ mean_alpha + Soil_depth + mean_PD, random = ~ 1|Site, data= div_ants2),
               lme(mean_alpha ~ Soil_depth, random = ~ 1|Site, data= div_ants2),
               mean_PD %~~% mean_FD
)


summary(SEM_A0) # best


SEM_A1 <- psem(lme(mean_PD ~ mean_alpha, random = ~ 1|Site, data= div_ants2),
               lme(mean_FD ~ mean_alpha + Soil_depth, random = ~ 1|Site, data= div_ants2),
               lme(mean_alpha ~ Soil_depth, random = ~1|Site, data= div_ants2))



summary(SEM_A1)




# Carabids

SEM_C0 <- psem(lme(mean_PD ~ mean_alpha + bare_ground + HILL_S, random = ~ 1|Site, data= div_car2),
               lme(mean_FD ~ mean_alpha + mean_PD + HILL_S, random = ~ 1|Site, data= div_car2),
               lme(mean_alpha ~ HILL_S, random = ~ 1|Site, data= div_car2),
               mean_PD %~~% mean_FD
)


summary(SEM_C0)
  

SEM_C1 <- psem(lme(mean_PD ~ mean_alpha + + HILL_S + bare_ground, random = ~ 1|Site, data= div_car2),
               lme(mean_FD ~ mean_PD, random = ~ 1|Site, data= div_car2),
               lme(mean_alpha ~ HILL_S, random = ~ 1|Site, data= div_car2)
)



summary(SEM_C1)

SEM_C3 <- psem(lme(mean_PD ~ mean_alpha + bare_ground + HILL_S, random = ~ 1|Site, data= div_car2),
               lme(mean_FD ~ mean_alpha + mean_PD + HILL_S + HILL_L, random = ~ 1|Site, data= div_car2),
               lme(mean_alpha ~ HILL_F, random = ~ 1|Site, data= div_car2),
               mean_PD %~~% mean_FD
)


summary(SEM_C3)
