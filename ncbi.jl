# Package installation

using Pkg
Pkg.add("Bio")

# Import Eutilities
using Bio.Services.EUtils

# Testing
p_pardo = Dict()
esearch(p_pardo, db ="Nucleotide", term = "Pardosa prativaga[ORGN]", usehistory = true)
print(p_pardo)
print(esummary(p_pardo))
